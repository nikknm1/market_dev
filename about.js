
$(document).ready(function() {
    const prevButt = $('.footer-back');
    const nextButt = $('.footer-next');
    const slider = $('.slider-container').slick({
        slidesToShow: 1,
        dots: false,
        infinite: false,
        nextArrow: nextButt,
        prevArrow: prevButt,
        autoplay: false,
    });

    const params = {
        opacity: 1,
    }

    $('.grid-title').css('opacity', 0);
    $('.about-first-block').css('opacity', 0);
    $('.grid-fifth-block').css('opacity', 0);
    $('.about-second-block').css('opacity', 0);
    $('.about-fourth-block').css('opacity', 0);
    $('.grid-six-block').css('opacity', 0);

    $('.grid-title').animate(params, 500, function() {
        $('.about-first-block').animate(params, 500, function() {
            $('.grid-fifth-block').animate(params, 500, function() {
                $('.about-second-block').animate(params, 500, function() {
                    $('.about-fourth-block').animate(params, 500, function() {
                        $('.grid-six-block').animate(params, 500);
                    });
                });
            });
        });
    });

    $.fn.animate_Text = function() {
        var string = this.text();
        return this.each(function(){
            var $this = $(this);
            $this.html(string.replace(/./g, '<span class="new">$&</span>'));
            $this.find('span.new').each(function(i, el){
                setTimeout(function(){ $(el).addClass('div_opacity'); }, 30 * i);
            });
        });
    };
    $('.slide-founded.slick-active .typed-text').show();
    $('.slide-founded.slick-active .typed-text').animate_Text();
});
