/* To avoid CSS expressions while still supporting IE 7 and IE 6, use this script */
/* The script tag referencing this file must be placed before the ending body tag. */

/* Use conditional comments in order to target IE 7 and older:
	<!--[if lt IE 8]><!-->
	<script src="ie7/ie7.js"></script>
	<!--<![endif]-->
*/

(function() {
	function addIcon(el, entity) {
		var html = el.innerHTML;
		el.innerHTML = '<span style="font-family: \'imoon\'">' + entity + '</span>' + html;
	}
	var icons = {
		'imoon-test-1': '&#xe908;',
		'imoon-test-2': '&#xe909;',
		'imoon-test-3': '&#xe90a;',
		'imoon-test-4': '&#xe90b;',
		'imoon-test-5': '&#xe90c;',
		'imoon-internet-magazin': '&#xe900;',
		'imoon-all': '&#xe901;',
		'imoon-visit': '&#xe902;',
		'imoon-corp_sites': '&#xe903;',
		'imoon-lp': '&#xe904;',
		'imoon-design': '&#xe905;',
		'imoon-in_process': '&#xe906;',
		'imoon-services': '&#xe907;',
		'0': 0
		},
		els = document.getElementsByTagName('*'),
		i, c, el;
	for (i = 0; ; i += 1) {
		el = els[i];
		if(!el) {
			break;
		}
		c = el.className;
		c = c.match(/imoon-[^\s'"]+/);
		if (c && icons[c[0]]) {
			addIcon(el, icons[c[0]]);
		}
	}
}());
