
$(document).ready(function() {
    const params = {
        opacity: 1,
    }

    $('.blog-item-container.one').css('opacity', 0);
    $('.blog-item-container.two').css('opacity', 0);
    $('.blog-item-container.three').css('opacity', 0);
    $('.blog-item-container.four').css('opacity', 0);
    $('.blog-item-container.five').css('opacity', 0);
    $('.blog-item-container.six').css('opacity', 0);

    $('.blog-item-container.one').animate(params, 500, function() {
        $('.blog-item-container.three').animate(params, 500, function() {
            $('.blog-item-container.five').animate(params, 500, function() {
                $('.blog-item-container.two').animate(params, 500, function() {
                    $('.blog-item-container.four').animate(params, 500, function() {
                        $('.blog-item-container.six').animate(params, 500);
                    });
                });
            });
        });
    });
});
