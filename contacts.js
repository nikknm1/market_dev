
$(document).ready(function() {
    const params = {
        opacity: 1,
    }

    $('.contacts-item-container.selected-city').css('opacity', 0);
    $('.contacts-item-container.one').css('opacity', 0);
    $('.contacts-item-container.two').css('opacity', 0);
    $('.contacts-item-container.three').css('opacity', 0);
    $('.contacts-item-container.four').css('opacity', 0);
    $('.contacts-item-container.five').css('opacity', 0);

    $('.contacts-item-container.selected-city').animate(params, 500, function() {
        $('.contacts-item-container.one').animate(params, 500, function() {
            $('.contacts-item-container.two').animate(params, 500, function() {
                $('.contacts-item-container.three').animate(params, 500, function() {
                    $('.contacts-item-container.four').animate(params, 500, function() {
                        $('.contacts-item-container.five').animate(params, 500);
                    });
                });
            });
        });
    });
});
