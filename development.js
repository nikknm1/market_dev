
$(document).ready(function() {
    /*const prevButt = $('.footer-back');
    const nextButt = $('.footer-next');
    const slider = $('.slider-container').slick({
        slidesToShow: 1,
        dots: false,
        infinite: false,
        nextArrow: nextButt,
        prevArrow: prevButt,
        autoplay: false,
    });*/
    const tabs = $( "#tabs" ).tabs();

    const params = {
        opacity: 1,
    }

    $('.grid-title').css('opacity', 0);
    $('.grid-sites').css('opacity', 0);
    $('.grid-services').css('opacity', 0);
    $('.grid-landing').css('opacity', 0);
    $('.grid-commerce').css('opacity', 0);
    $('.grid-examples').css('opacity', 0);

    $('.grid-title').animate(params, 500, function() {
        $('.grid-sites').animate(params, 500, function() {
            $('.grid-services').animate(params, 500, function() {
                $('.grid-landing').animate(params, 500, function() {
                    $('.grid-commerce').animate(params, 500, function() {
                        $('.grid-examples').animate(params, 500);
                    });
                });
            });
        });
    });

    $.fn.animate_Text = function() {
        var string = this.text();
        return this.each(function(){
            var $this = $(this);
            $this.html(string.replace(/./g, '<span class="new">$&</span>'));
            $this.find('span.new').each(function(i, el){
                setTimeout(function(){ $(el).addClass('div_opacity'); }, 40 * i);
            });
        });
    };
    $('.development .typed-text').show();
    $('.development .typed-text').animate_Text();
});
