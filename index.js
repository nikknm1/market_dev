$(document).ready(function() {

    const params = {
        opacity: 1,
    }

    $('.grid-marketing').css('opacity', 0);
    $('.header-themes').css('opacity', 0);
    $('.grid-design').css('opacity', 0);
    $('.grid-web').css('opacity', 0);
    $('.grid-app').css('opacity', 0);
    $('.grid-seo').css('opacity', 0);
    $('.grid-breess').css('opacity', 0);
    $('.footer-partners').css('opacity', 0);
    $('.footer-back').css('opacity', 0);
    $('.footer-next').css('opacity', 0);

    $('.grid-marketing').animate(params, 500, function() {
        $('.header-themes').animate(params, 500, function() {
            $('.grid-design').animate(params, 500, function() {
                $('.grid-web').animate(params, 500, function() {
                    $('.grid-app').animate(params, 500, function() {
                        $('.grid-seo').animate(params, 500, function() {
                            $('.grid-breess').animate(params, 500);
                            $('.footer-partners').animate(params, 500);
                            $('.footer-back').animate(params, 500);
                            $('.footer-next').animate(params, 500);
                        });
                    });
                });
            });
        });
    });
    $(document).ready(function(){
        $.fn.animate_Text = function() {
            var string = this.text();
            return this.each(function(){
                var $this = $(this);
                $this.html(string.replace(/./g, '<span class="new">$&</span>'));
                $this.find('span.new').each(function(i, el){
                    setTimeout(function(){ $(el).addClass('div_opacity'); }, 40 * i);
                });
            });
        };
        $('.grid-marketing .typed-text').show();
        $('.grid-marketing .typed-text').animate_Text();
    });


});
