var Chatra = function() {
    alert('lets chat');
}

$(document).ready(function() {

    $('#light-theme').click(function() {
        $('#main').removeClass( "dark light" );
        $('#main').addClass( "light" );
    });
    $('#dark-theme').click(function() {
        $('#main').removeClass( "dark light" );
        $('#main').addClass( "dark" );
    });

    function blurGrids() {
        $ (" .grid").css( "opacity", "0.1" );
        $ (" .header-themes").css( "opacity", "0.1" );
        $ (" .footer-partners").css( "opacity", "0.1" );
        $ (" .footer-back").css( "opacity", "0.1" );
        $ (" .footer-next").css( "opacity", "0.1" );
        $ (" .price-body").css( "opacity", "0.1" );

    }
    function focusGrids() {
        $ (" .grid").css( "opacity", "initial" );
        $ (" .header-themes").css( "opacity", "initial" );
        $ (" .footer-partners").css( "opacity", "initial" );
        $ (" .footer-back").css( "opacity", "initial" );
        $ (" .footer-next").css( "opacity", "initial" );
        $ (" .price-body").css( "opacity", "initial" );
    }
    function showMainMenu() {
        $( ".menu-theme-bottom-button" ).css( "border-bottom-right-radius", "0" );
        $( ".header-menu-contact" ).css( "display", "none" );
        $( ".header-menu-main" ).css( "display", "block" );
        $( ".header-menu" ).last().addClass( "selected-menu" );
        blurGrids();
    }
    function hideMainMenu() {
        $( ".menu-theme-bottom-button" ).css( "border-bottom-right-radius", "12px" );
        $( ".header-menu-main" ).css( "display", "none" );
        $( ".header-menu" ).last().removeClass( "selected-menu" );
        focusGrids()
    }

    function showContactMenu() {
        $( ".dropdown-contacts" ).css( "border-bottom-right-radius", "0" );
        $( ".header-menu-contact" ).css( "display", "block" );
        $( ".header-menu-main" ).css( "display", "none" );
        $( ".header-menu" ).last().removeClass( "selected-menu" );
        $( ".header-contacts-phone" ).last().addClass( "selected-contact-menu" );
        blurGrids();
    }
    function hideContactMenu() {
        $( ".header-menu-contact" ).css( "display", "none" );
        $( ".header-contacts-phone" ).last().removeClass( "selected-contact-menu" );
        focusGrids()
    }

    function showMapMenu() {
        $( ".footer-map" ).last().addClass( "selected-map-menu" );
        blurGrids();
    }
    function hideMapMenu() {
        $( ".footer-map" ).last().removeClass( "selected-map-menu" );
        focusGrids()
    }
    function showChatMenu() {
        $( ".footer-tea" ).last().addClass( "selected-chat-menu" );
        blurGrids();
    }
    function hideChatMenu() {
        $( ".footer-tea" ).last().removeClass( "selected-chat-menu" );
        focusGrids()
    }


    // кнопка главного меню
    let firstMainMenuClick = true;
    $('.burger-menu').hover(function(){
        hideContactMenu();
        showMainMenu();
        if ($('.header-menu').hasClass('header-menu')) {
            $( ".header-menu .header-contacts-phone" ).css( "border-bottom-right-radius", "0" );
        }
        firstMainMenuClick = true;
    });
    $('.header-menu').hover(function(){
    }, function(){
        hideMainMenu();
        $( ".header-menu .header-contacts-phone" ).css( "border-bottom-right-radius", "" );
    });
    $('.burger-menu').click(function() {
        if (!firstMainMenuClick) {
            if ($('.header-menu').hasClass('header-menu')) {
                $( ".header-menu .header-contacts-phone" ).css( "border-bottom-right-radius", "0" );
            }
            if ($( ".header-menu" ).hasClass( "selected-menu" )) {
                hideMainMenu();
                $( ".header-menu .header-contacts-phone" ).css( "border-bottom-right-radius", "" );
            } else {
                showMainMenu();
            }
        }
        firstMainMenuClick = false;
    });
    $('.burger-menu').on('tap', function() {
        if (!firstMainMenuClick) {
            if ($('.header-menu').hasClass('header-menu')) {
                $( ".header-contacts-phone" ).css( "border-bottom-right-radius", "0" );
            }
            if ($( ".header-menu" ).hasClass( "selected-menu" )) {
                hideMainMenu();
                $( ".header-contacts-phone" ).css( "border-bottom-right-radius", "" );
            } else {
                showMainMenu();
            }
        }
        firstMainMenuClick = false;
    });


    // кнопка контактов
    let firstContactMenuClick = true;
    $('.dropdown-contacts').hover(function(el){
        firstContactMenuClick = true;
        hideMainMenu();
        showContactMenu();
        if ($(el.currentTarget).parent().parent().hasClass('header-menu')) {
            $( ".burger-menu" ).css( "border-bottom-left-radius", "0" );
        }
        $('.header-contacts-phone').parent().one('mouseleave', function() {
            hideContactMenu();
            $( ".burger-menu" ).css( "border-bottom-left-radius", "" );
        });
    });
    $('.dropdown-contacts').click(function() {
        if (!firstContactMenuClick) {
            if ($( ".header-contacts-phone" ).hasClass( "selected-contact-menu" )) {
                hideContactMenu();
                $( ".burger-menu" ).css( "border-bottom-left-radius", "" );
            } else {
                showContactMenu();
                $( ".burger-menu" ).css( "border-bottom-left-radius", "0" );
            }
        }
        firstContactMenuClick = false;
    });
    $('.dropdown-contacts').on('tap',function() {
        if (!firstContactMenuClick) {
            if ($( ".header-contacts-phone" ).hasClass( "selected-contact-menu" )) {
                hideContactMenu();
                $( ".burger-menu" ).css( "border-bottom-left-radius", "" );
            } else {
                showContactMenu();
                $( ".burger-menu" ).css( "border-bottom-left-radius", "0" );
            }
        }
        firstContactMenuClick = false;
    });


    // кнопка карты
    let firstMapMenuClick = true;
    $('.footer-map').hover(function(){
        firstMapMenuClick = true;
        hideChatMenu();
        showMapMenu();
        $('.footer-map').one('mouseleave', function() {
            hideMapMenu();
        });
    });
    $('.footer-map').click(function() {
        if (!firstMapMenuClick) {
            if ($( ".footer-map" ).hasClass( "selected-map-menu" )) {
                hideMapMenu();
            } else {
                hideChatMenu();
                showMapMenu();
            }
        }
        firstMapMenuClick = false;
    });
    $('.footer-map').on('tap',function() {
        if (!firstMapMenuClick) {
            if ($( ".footer-map" ).hasClass( "selected-map-menu" )) {
                hideMapMenu();
            } else {
                hideChatMenu();
                showMapMenu();
            }
        }
        firstMapMenuClick = false;
    });


    // кнопка карты
    let firstChatMenuClick = true;
    $('.footer-tea').hover(function(){
        firstChatMenuClick = true;
        hideMapMenu();
        showChatMenu();
        $('.footer-tea').one('mouseleave', function() {
            hideChatMenu();
        });
    });
    $('.footer-tea').click(function() {
        if (!firstChatMenuClick) {
            if ($( ".footer-tea" ).hasClass( "selected-chat-menu" )) {
                hideChatMenu();
            } else {
                hideMapMenu();
                showChatMenu();
            }
        }
        firstChatMenuClick = false;
    });
    $('.footer-tea').on('tap',function() {
        if (!firstChatMenuClick) {
            if ($( ".footer-tea" ).hasClass( "selected-chat-menu" )) {
                hideChatMenu();
            } else {
                hideMapMenu();
                showChatMenu();
            }
        }
        firstChatMenuClick = false;
    });

    $( '#main' ).scroll(function() {
        hideMapMenu();
        hideChatMenu();
        hideContactMenu();
        hideMainMenu();
    });
    if ( Math.abs($( window ).width()/$( window ).height()) > 2) {
        const h1 = $('h1').css( "font-size", '6vh' );
        const h2 = $('h2').css( "font-size", '4.8vh' );
        const titleText = $('.grid-title-text').css( "font-size", '2.5vh' );
        const containerText = $('.grid-container .text, .grid-item-content-text').css( "font-size", '0.9vw' );
    } else {
        const h1 = $('h1').css( "font-size", '' );
        const h2 = $('h2').css( "font-size", '' );
        const titleText = $('.grid-title-text').css( "font-size", '' );
        const containerText = $('.grid-container .text, .grid-item-content-text').css( "font-size", '' );
    }
});
$(window).resize(function() {
    if ( Math.abs($( window ).width()/$( window ).height()) > 2) {
        const h1 = $('h1').css( "font-size", '6vh' );
        const h2 = $('h2').css( "font-size", '4.8vh' );
        const titleText = $('.grid-title-text').css( "font-size", '2.5vh' );
        const containerText = $('.grid-container .text, .grid-item-content-text').css( "font-size", '0.9vw' );
    } else {
        const h1 = $('h1').css( "font-size", '' );
        const h2 = $('h2').css( "font-size", '' );
        const titleText = $('.grid-title-text').css( "font-size", '' );
        const containerText = $('.grid-container .text, .grid-item-content-text').css( "font-size", '' );
    }
});
