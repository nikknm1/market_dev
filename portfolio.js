
$(document).ready(function() {
    const params = {
        opacity: 1,
    }

    $('.portfolio-item-container.one').css('opacity', 0);
    $('.portfolio-item-container.two').css('opacity', 0);
    $('.portfolio-item-container.three').css('opacity', 0);
    $('.portfolio-item-container.four').css('opacity', 0);
    $('.portfolio-item-container.five').css('opacity', 0);
    $('.portfolio-item-container.six').css('opacity', 0);

    $('.portfolio-item-container.one').animate(params, 500, function() {
        $('.portfolio-item-container.three').animate(params, 500, function() {
            $('.portfolio-item-container.five').animate(params, 500, function() {
                $('.portfolio-item-container.two').animate(params, 500, function() {
                    $('.portfolio-item-container.four').animate(params, 500, function() {
                        $('.portfolio-item-container.six').animate(params, 500);
                    });
                });
            });
        });
    });
});
