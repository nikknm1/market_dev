
$(document).ready(function() {
    const params = {
        opacity: 1,
    }

    $('.grid-container').css('opacity', 0);

    $('.grid-container').animate(params, 500);
});
