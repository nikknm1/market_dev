
$(document).ready(function() {
    const params = {
        opacity: 1,
    }

    $('.grid-title').css('opacity', 0);
    $('.webdesign-logotypes').css('opacity', 0);
    $('.grid-project').css('opacity', 0);
    $('.grid-catalog').css('opacity', 0);
    $('.grid-3d').css('opacity', 0);
    $('.grid-request').css('opacity', 0);

    $('.grid-title').animate(params, 500, function() {
        $('.webdesign-logotypes').animate(params, 500, function() {
            $('.grid-project').animate(params, 500, function() {
                $('.grid-catalog').animate(params, 500, function() {
                    $('.grid-3d').animate(params, 500, function() {
                        $('.grid-request').animate(params, 500);
                    });
                });
            });
        });
    });

    $(document).ready(function(){
        $.fn.animate_Text = function() {
            var string = this.text();
            return this.each(function(){
                var $this = $(this);
                $this.html(string.replace(/./g, '<span class="new">$&</span>'));
                $this.find('span.new').each(function(i, el){
                    setTimeout(function(){ $(el).addClass('div_opacity'); }, 40 * i);
                });
            });
        };
        $('.webdesign .typed-text').show();
        $('.webdesign .typed-text').animate_Text();
    });
});
